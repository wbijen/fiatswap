import { createSlice } from "@reduxjs/toolkit";
import * as Localization from "expo-localization";

export const conversionSlice = createSlice({
  name: "conversion",
  reducerPath: "conversion",
  initialState: {
    amount: 1,
    base: Localization.currency,
    quote: Localization.currency === "EUR" ? "USD" : "EUR",
    rates: [],
  },
  reducers: {
    setAmount: (state, action) => {
      state.amount = action.payload;
    },
    setBase: (state, action) => {
      console.log(action);
      state.base = action.payload;
    },
    setQuote: (state, action) => {
      state.quote = action.payload;
    },
    setRates: (state, action) => {
      state.rates = action.payload;
    },
    swapPairs: (state) => {
      const quote = state.quote;
      state.quote = state.base;
      state.base = quote;
    },
  },
});
export const { setAmount, setBase, setQuote, setRates, swapPairs } = conversionSlice.actions;
export default conversionSlice.reducer;
