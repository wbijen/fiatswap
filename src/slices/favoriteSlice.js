import { createSlice } from "@reduxjs/toolkit";
import * as Localization from "expo-localization";
import { generateId } from "../utils/text";

export const favoriteSlice = createSlice({
  name: "favorite",
  reducerPath: "favorite",
  initialState: {
    favorites: [{
      id: "adlk1adf23",
      base: Localization.currency,
      quote: Localization.currency === "EUR" ? "USD" : "EUR",
      amount: 1,
    }, {
      base: "BTC",
      quote: Localization.currency,
      amount: 1,
      id: "asdf39asdf",
    }],
  },
  reducers: {
    addFavorite: (state, action) => {
      state.favorites.unshift({
        id: generateId(10),
        base: action.payload.base,
        quote: action.payload.quote,
        amount: action.payload.amount,
      });
    },
    removeFavorite: (state, action) => {
      state.favorites = state.favorites.filter((item) => item.id !== action.payload);
    },
  },
});
export const { addFavorite, removeFavorite } = favoriteSlice.actions;
export default favoriteSlice.reducer;
