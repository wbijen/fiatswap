const light = {
  dark: "#231d54",
  primary: "#8100ff",
  secondary: "#9388db",
  item: "#f7f7fb",
  highlight: "#cccccc",
  background: "#f6f6f6",
  surface: "#ffffff",
  accent: "#f1c40f",
  backdrop: "#a7a0d5",
  borderColor: "#a7a0d5",
};

export default light;
