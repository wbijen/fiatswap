import { StatusBar } from "expo-status-bar";
import React from "react";
import { Provider } from "react-redux";
import { persistor, store } from "./store";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import ConvertCurrencies from "./scene/convertCurrencies";
import { colors } from "./theme";
import { PersistGate } from "redux-persist/integration/react";

const theme = {
  ...DefaultTheme,
  roundness: 8,
  colors: {
    ...DefaultTheme.colors,
    colors,
  },
};

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider theme={theme}>
          <ConvertCurrencies />
          <StatusBar />
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
}
