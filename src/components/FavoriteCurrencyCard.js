import * as React from "react";
import { Card, Paragraph, Title } from "react-native-paper";
import {  StyleSheet, View } from "react-native";
import { useExchangeRateConversion } from "../services/exchangeRates/ExchangeRateHook";
import getSymbolFromCurrency from "currency-symbol-map";

const FavoriteCurrencyCard = ({ base, quote, amount }) => {
  const { output } = useExchangeRateConversion(base, quote, amount);
  const baseSymbol = getSymbolFromCurrency(base) || ''
  const quoteSymbol = getSymbolFromCurrency(quote) || ""

  return (
      <Card style={styles.Card}>
        <Card.Content>
          <View style={styles.currenciesContainer}>
            <Title>{base}</Title>
            <Title>{quote}</Title>
          </View>
          <View style={styles.currenciesContainer}>
            <Paragraph>{`${baseSymbol} ${amount}`}</Paragraph>
            <Paragraph>{`${quoteSymbol} ${output || "-"}`}</Paragraph>
          </View>
        </Card.Content>
      </Card>
  );
};


const styles = StyleSheet.create({
  Card: { marginVertical: 10 },
  currenciesContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  buttonLabel: {
    fontSize: 11,
  }
});

export default FavoriteCurrencyCard;
