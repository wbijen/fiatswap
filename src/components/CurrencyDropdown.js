import { useGetSymbolsQuery } from "../services/exchangeRates";
import { StyleSheet } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import React, { useEffect, useState } from "react";
import { colors } from "../theme";
import { ActivityIndicator } from "react-native-paper";

export const CurrencyDropdown = ({ value, onSelect, style }) => {
  const { data, isLoading } = useGetSymbolsQuery();
  const symbolMap = data?.symbols || {};
  const symbolsArr = Object.keys(symbolMap).map((key) => ({ label: `(${key}) ${symbolMap[key]}`, value: key }));
  const [isOpen, setOpen] = useState(false);
  const [privateValue, setPrivateValue] = useState(null);

  useEffect(() => {
    setPrivateValue(value);
  }, [value, setPrivateValue]);

  const setValue = (newValue) => {
    setPrivateValue(newValue);
    onSelect(newValue());
  };

  if (isLoading) {
    return <ActivityIndicator />;
  }


  return (
    <DropDownPicker
      style={[styles.dropdown, style]}
      loading={isLoading}
      items={symbolsArr}
      value={privateValue}
      setValue={setValue}
      open={isOpen}
      modalProps={{ onRequestClose: () => setOpen(false) }} //back button support
      searchable={true}
      listMode="MODAL"
      setOpen={setOpen}
    />
  );
};

const styles = StyleSheet.create({
  dropdown: {
    borderColor: colors.borderColor,
  }
});
