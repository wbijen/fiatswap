import { StyleSheet, Text, View } from "react-native";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import React, { useEffect, useRef, useState } from "react";
import { colors } from "../theme";
import { useGetRatesQuery } from "../services/exchangeRates";

const RatesUpdateMessage = () => {
  const ratesQuery = useGetRatesQuery();
  const [date, setDate] = useState("");
  const timerRef = useRef();

  //Update when rates are updated, also update
  //every 10 seconds because the rates formatDistanceToNow needs updating
  useEffect(() => {
    updateDate();
    timerRef.current = createInterval(10000);
    return () => clearInterval(timerRef.current);
  }, [ratesQuery, setDate]);

  const createInterval = (ms) => {
    return setInterval(() => {
      updateDate();
      clearInterval(timerRef.current);
    }, ms);
  };

  const updateDate = () => {
    setDate(!!ratesQuery?.fulfilledTimeStamp ? formatDistanceToNow(ratesQuery.fulfilledTimeStamp, { addSuffix: true }) : "-");
  };

  return (
    <View style={styles.updateContainer}>
      <Text
        style={styles.updateText}>{`Rates are from ${date}`}</Text>
    </View>
  );
};

export default RatesUpdateMessage;

const styles = StyleSheet.create({
  updateText: {
    color: colors.secondary,
  },
  updateContainer: {
    marginVertical: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
