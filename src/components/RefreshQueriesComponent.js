import { RefreshControl, ScrollView } from "react-native";
import React, { useEffect } from "react";
import { useGetRatesQuery, useGetSymbolsQuery } from "../services/exchangeRates";

/**
 * Component that provides swipe down to refresh functionality
 * @param style
 * @param children
 * @return {JSX.Element}
 * @constructor
 */
const RefreshQueriesComponent = ({ style, children }) => {
  const symbolsQuery = useGetSymbolsQuery(); //refresh every hour
  const ratesQuery = useGetRatesQuery();

  //refresh on start
  useEffect(() => {
    symbolsQuery.refetch();
    ratesQuery.refetch();
  },[])

  return (
    <ScrollView style={style} scrollEnabled={false} refreshControl={
      <RefreshControl
        refreshing={symbolsQuery.isFetching || ratesQuery.isFetching}
        onRefresh={() => {
          ratesQuery.refetch();
          symbolsQuery.refetch();
        }}
      />
    }>
      {children}
    </ScrollView>
  );
};

export default RefreshQueriesComponent;
