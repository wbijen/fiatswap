import { TextInput } from "react-native-paper";
import { StyleSheet } from "react-native";
import React from "react";
import { colors } from "../theme";
import getSymbolFromCurrency from "currency-symbol-map";
import CurrencyInput from 'react-native-currency-input';
import * as Localization from 'expo-localization';

export const AmountInput = ({ value, currency, setValue, editable = true }) => {
    const symbol = getSymbolFromCurrency(currency) || "";


    return (
      <TextInput
        style={styles.currencyInput}
        label="Amount"
        render={(props) => (
          <CurrencyInput
            {...props}
            delimiter={Localization.digitGroupingSeparator}
            separator={Localization.decimalSeparator}
            onChangeValue={setValue}
            prefix={symbol + " "}
            precision={2}
            minValue={0}
          />
        )}
        value={value}
        //onChangeText={text => setValue(text)}
        mode={"outlined"}
        editable={editable}
        outlineColor={colors.borderColor}
      />
    );
  }
;

const styles = StyleSheet.create({
  currencyInput: {
    width: 150,
    backgroundColor: "#ffffff",
  },
});
