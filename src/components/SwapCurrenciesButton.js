import { FontAwesome } from "@expo/vector-icons";
import { colors } from "../theme";
import React from "react";
import { TouchableOpacity } from "react-native";

export const SwapCurrenciesButton = ({ onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <FontAwesome name="exchange" color={colors.primary} size={30} />
    </TouchableOpacity>
  );
};
