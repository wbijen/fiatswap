import { TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../theme";
import React from "react";

const FavoriteButton = ({ onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <MaterialIcons name="favorite-outline" size={30} color={colors.primary} />
    </TouchableOpacity>
  );
};

export default FavoriteButton;
