import React from "react";
import { StyleSheet } from "react-native";
import { AmountInput } from "../../components/AmountInput";
import { useSelector } from "react-redux";
import { ActivityIndicator } from "react-native-paper";
import { useExchangeRateConversion } from "../../services/exchangeRates/ExchangeRateHook";

export const CurrencyAmountOutput = () => {
  const base = useSelector((state) => state.conversion.base);
  const quote = useSelector((state) => state.conversion.quote);
  const inputAmount = useSelector((state) => state.conversion.amount);
  const { output, loading } = useExchangeRateConversion(base, quote, inputAmount);

  if (loading) {
    return <ActivityIndicator style={styles.container} />;
  }

  return (<AmountInput currency={quote} editable={false} value={output || `0`} />);
};

const styles = StyleSheet.create({
  container: {
    width: 140,
  },
});
