import { SafeAreaView, StyleSheet, View } from "react-native";
import React from "react";
import LogoComponent from "../../components/LogoComponent";
import { colors } from "../../theme";
import { Divider } from "react-native-paper";
import RefreshQueriesComponent from "../../components/RefreshQueriesComponent";
import FavoriteCurrencies from "./FavoriteCurrencies";
import CurrenciesConverterActions from "./CurrenciesConverterActions";
import RatesUpdateMessage from "../../components/RatesUpdateMessage";

const ConvertCurrencies = () => {
  return (
    <SafeAreaView style={styles.container}>
      <RefreshQueriesComponent style={styles.topContainer}>
        <View style={styles.logo}>
          <LogoComponent width={200} />
        </View>
        <CurrenciesConverterActions />
        <Divider style={styles.divider} />
      </RefreshQueriesComponent>
      <FavoriteCurrencies style={styles.bottomContainer} />
      <RatesUpdateMessage />
    </SafeAreaView>
  );
};

export default ConvertCurrencies;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: 20,
    backgroundColor: colors.background,
  },
  topContainer: {
    flexShrink: 1,
    flexBasis: "auto",
    flexGrow: 0,
  },
  bottomContainer: {
    flex: 3,
  },
  logo: {
    marginTop: 50,
    alignItems: "center",
    justifyContent: "center",
  },

  divider: {
    marginTop: 20,
    marginBottom: 10,
  },
});
