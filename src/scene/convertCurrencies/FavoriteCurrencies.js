import { useDispatch, useSelector } from "react-redux";
import { Animated, Dimensions, StyleSheet, Text, View } from "react-native";
import FavoriteCurrencyCard from "../../components/FavoriteCurrencyCard";
import { removeFavorite } from "../../slices/favoriteSlice";
import { FontAwesome } from "@expo/vector-icons";
import { colors } from "../../theme";
import React, { useRef } from "react";
import { SwipeListView } from "react-native-swipe-list-view";

const FavoriteCurrencies = ({ style }) => {
  const arr = useSelector((state) => state.favorite.favorites);
  const dispatch = useDispatch();
  const screenWidth = Dimensions.get("window").width;
  const animationIsRunning = useRef(false);

  const rowTranslateAnimatedValues = {};
  arr.forEach((obj) => {
    rowTranslateAnimatedValues[obj.id] = new Animated.Value(1);
  })

  const onDelete = (id) => {
    dispatch(removeFavorite(id));
  };

  const onSwipeValueChange = swipeData => {
    const { key, value } = swipeData;
    if (
      value < -Dimensions.get('window').width &&
      !animationIsRunning.current
    ) {
      animationIsRunning.current = true;
      Animated.timing(rowTranslateAnimatedValues[key], {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {
        onDelete(key)
        animationIsRunning.current = false;
      });
    }
  };

  const renderHiddenItem = () => (
    <View style={styles.standaloneRowBack}>
      <Text />
      <Text style={styles.backTextWhite}>Delete</Text>
    </View>
  );

  const renderItem = (data) => {
    return (
    <Animated.View
      style={
        {
          height: rowTranslateAnimatedValues[
            data.item.key
            ].interpolate({
            inputRange: [0, 1],
            outputRange: [0, 108],
          }),
        }}
    >
      <FavoriteCurrencyCard key={data.item.id} {...data.item} />
    </Animated.View>
  )};
  return (
    <>
      <View style={styles.swipeMessage}>
        <FontAwesome name="long-arrow-left" size={24} color={colors.secondary} />
        <Text style={styles.swipeText}>Swipe left to remove favorite</Text>
      </View>
      <SwipeListView
        useFlatList={true}
        data={arr.map((item) => ({...item, key : item.id}))}
        renderHiddenItem={renderHiddenItem}
        disableRightSwipe
        leftOpenValue={0}
        rightOpenValue={-screenWidth}
        onSwipeValueChange={onSwipeValueChange}
        renderItem={renderItem}
        style={style}
        useNativeDriver={false}
      />
    </>
  )
    ;
};

const styles = StyleSheet.create({
  scrollView: {
    justifyContent: "flex-start",
  },
  swipeMessage: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    width: "100%",
  },
  swipeText: {
    marginLeft: 10,
    color: colors.secondary,
  },
  standaloneRowBack: {
    alignItems: "center",
    backgroundColor: "#f55858",
    flex: 1,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 15,
  },
  backTextWhite: {
    marginRight: 20,
    color: "#FFF",
  },
});

export default FavoriteCurrencies;
