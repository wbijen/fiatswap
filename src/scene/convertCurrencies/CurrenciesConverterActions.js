import { useDispatch, useSelector } from "react-redux";
import { StyleSheet, View } from "react-native";
import { CurrencyDropdown } from "../../components/CurrencyDropdown";
import { setAmount, setBase, setQuote, swapPairs } from "../../slices/conversionSlice";
import { SwapCurrenciesButton } from "../../components/SwapCurrenciesButton";
import { AmountInput } from "../../components/AmountInput";
import FavoriteButton from "../../components/FavoriteButton";
import { addFavorite } from "../../slices/favoriteSlice";
import { CurrencyAmountOutput } from "./CurrencyAmountOutput";
import React from "react";

const CurrenciesConverterActions = () => {
  const amount = useSelector((state) => state.conversion.amount);
  const base = useSelector((state) => state.conversion.base);
  const quote = useSelector((state) => state.conversion.quote);
  const dispatch = useDispatch();

  return (
    <View style={styles.currencySelect}>
      <View style={styles.rowContainer}>
        <View style={styles.dropdownContainer}>
          <CurrencyDropdown style={styles.currencyDropdown} onSelect={(base) => dispatch(setBase(base))}
                            value={base} />
        </View>
        <SwapCurrenciesButton onPress={() => dispatch(swapPairs())} />
        <View style={styles.dropdownContainer}>
          <CurrencyDropdown style={styles.currencyDropdown} onSelect={(quote) => dispatch(setQuote(quote))}
                            value={quote} />
        </View>
      </View>
      <View style={styles.rowContainer}>
        <AmountInput currency={base} setValue={(number) => dispatch(setAmount(number))} value={amount} />
        <FavoriteButton onPress={() => dispatch(addFavorite({ base, quote, amount }))} />
        <CurrencyAmountOutput />
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  currencySelect: {
    // flexDirection: 'row',
    // justifyContent: 'space-around',
    alignItems: "center",
    width: "100%",
  },
  rowContainer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  dropdownContainer: {
    width: 150,
    alignItems: "center",
  },
  currencyDropdown: {
    height: 65,
  },
});


export default CurrenciesConverterActions;
