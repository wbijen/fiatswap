import { useGetRatesQuery } from "./index";
import React, { useEffect, useState } from "react";

export const useExchangeRateConversion = (base, quote, amount) => {
  const { data, error, isLoading } = useGetRatesQuery();
  const ratesInApiEURBase = data?.rates;
  const [output, setOutput] = useState(1);

  useEffect(() => {
    if (ratesInApiEURBase) {
      const output = amount / ratesInApiEURBase[base] * ratesInApiEURBase[quote];
      const roundedOutput = Math.round((output + Number.EPSILON) * 100) / 100;
      setOutput(roundedOutput);
    }
  }, [ratesInApiEURBase, base, quote, amount]);

  return { output, loading: !data || isLoading, error };
};
