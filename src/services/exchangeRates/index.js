import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import Constants from "expo-constants";

// Define a service using a base URL and expected endpoints
// Todo You might not want to expose the api key in a real production env
export const ratesApi = createApi({
  reducerPath: "ratesApi",
  baseQuery: fetchBaseQuery({ baseUrl: "http://api.exchangeratesapi.io/v1/" }),
  endpoints: (builder) => ({
    getSymbols: builder.query({
      query: () => `symbols?access_key=${Constants.manifest.extra.exchangeRatesApiKey}`,
    }),
    getRates: builder.query({
      query: () => `latest?access_key=${Constants.manifest.extra.exchangeRatesApiKey}`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetSymbolsQuery, useGetRatesQuery } = ratesApi;
